## Sage Notebook files

#sage_notebook.ipynb:
the code contains implementations of:

- babystep-giantstep algorithm
- pollard rho algorithm
- pohlig-hellman algorithm
- some other helpful functions

for sagemath (-> http://www.sagemath.org/).
It can be imported into a sagemath notebook.

# pollard_rho.sage:
- This file can be loaded without a notebook-gui, so you can run it on a server.
- It automatically tests the runtime of the algorithms. 
- This is mostly the same file as the sage_notebook.ipynb but without the plotting. 
- Usage: edit the test-settings in the auto_test() function beginning at line 778 and start the tests with "sage pollard_rho.sage"


# .csv files and .ods

these files contain the results of the runtime tests as .csv file and for LibreOffice Calc
