
# coding: utf-8

# In[1]:


import time
global delay

# delay zwischen den einzelen Berechnungsschritten
delay = 0
# g = generator of Z*_p
# p = prime
g = 20
p = 2903


# safe prime: q = 2*p +1 und p ist ebenfalls Primzahl
# In der Primen Restklassengruppe zu einer sicheren Primzahl kann
# jedes Element nur die Ordnung 1,2,p oder 2*p haben
# 2698727 ist eine 22-bit Primzahl
p_safe = 2698727
q_safe = 1349363
g_safe = 5


# In[2]:


# slow_powermod function: 
# by setting global delay you can
# artificially slow down calculations
# to see differences between the
# algorithms although using small primes
# input:
# int n, int g and int p with
# calculates g^n (mod p)

def slow_powermod(n,g,p):
    global delay
    res = g.powermod(n,p)
    sleep(delay)
    return res


# In[3]:


# brute force function:
# look for a solution by simply testing
# all possible values
# input:
# int x, int g, int p 
# where p is a prime number
# and g is a generator of
# Z*_p

def brute_force(x, g, p):
    i = 1
    while slow_powermod(i,g,p) != x:
        i += 1
        if i > p:
            return -1
    return i


# In[4]:


# babystep_giantstep function:
# calculates the discrete logarithm
# by using the babystep giantstep algorithm
# you can use the 'interaction' flag to turn 
# on/off the print outs
# for small primes it  can be nice to track
# the calculations, for bigger primes it soon
# gets confusing
# input:
# int a, int g, int g 
# where
# p is prime and g is a generator of Z*_p
# a is the value we want to calculate the 
# discrete logarithm for.
# returns value x for which g^x = a mod p 


def babystep_giantstep(a, g, p, interaction=False):
    if p not in Primes():
        print(" warning: %s is not prime!" %p)
    n = euler_phi(p)
    m = int(float(sqrt(n)))+1    # manuelle implementation der Aufrunden-Funktion
    
    if interaction:
        print("- - - - - - - - - - - - - - -")
        print("Babystep-Giantstep Algorithm")
        print("- - - - - - - - - - - - - - -")
        print(" try to find x with %s^x = %s (mod %s)" % (g, a, p))
        print(" |G| = %s" % n)
        print("  m = %s" % m)
        print "\n giantsteps j:"
        
    if a == g:
        return 1
        
    # berechne die giantstep Tabelle:
    giantsteps = {}
    for j in srange(0,m):
        value = slow_powermod(j,g,p)
        giantsteps[value] = j
        if interaction:
            print(" %s: %s^%s =  %s (mod %s)" %(j, g, j, value, p))
    
    if interaction:
        raw_input("press Enter to continue...")
        
    y = slow_powermod(-m,g,p)
    if interaction:
        print("")
        print("calculating babysteps now")
        print(" starting with \n y = %s = %g^(-%s) (mod %s) \n" % (y, g, m, p))
        raw_input("press Enter to continue...")
    
    # bereche giantsteps:
    for i in srange(0, m):
        y_ = mod(a * slow_powermod(i,y,p),p)
        
        if interaction:
            print(" %s: %s*%s^%s =  %s (mod %s)" % (i, a, y, i, y_, p))
            
        if y_ == 0:
            continue
        if y_ in giantsteps:
            j = giantsteps[y_]
            res = i*m + j
            if interaction:
                print("->>> %s is in giantsteps-list at position %s, we found our x" % (y_, j))
                print("->>> x = %s * %s + %s = %s" % (i, m, j, res))
            return res
    print("did not find a solution")
    return -1


# In[5]:


# solve_mod function:
# calculates the solutions for
#      s*X = t mod n
# you can use the 'interaction'-flag
# to get some more detailed output
# input: int s, t, n
# returns a list of all solutions
# smaller than the given n
# returns -1 if no solutions is given


def solve_mod(s, t, n, interaction=False):
    # solves equations s*x = t (mod n)
    if interaction:
        print("- - - - -")
        print("solve %s*X = %s (mod %s):" % (s, t, n))
        print("- - - - -")
    d = gcd(s, n)
    if t != 0:
        if d not in divisors(t):
            if interaction:
                print("no solution for  %s = %s mod %s" % (s,t,n))
                print("%s = gcd(%s, %s) is no divisor of %s" % (d, s,n, t))
            return -1
    
    s_ = int(s/d)   # we must cast type<int> here because the division normally returns a rational 
    t_ = int(t/d)   # for which we cannot compute the inverse_mod()
    n_ = int(n/d)
    
    if interaction:
        print("   gcd of %s and %s is: %s" % (n, s, d))
        print("   divided by gcd, new s',t', n':\n   -> %s*x = %s (mod %s)" % (s_, t_, n_))

    
    inv_ = inverse_mod(s_, n_)
    x = int(mod(inv_*t_, n_))
    res = []
    while x < n:
        res.append(x)
        x += n_
    if interaction:
        print("solutions are: %s" % res)
        print("- - - - -")
    return res


# In[6]:


# Step-class:
# a simple class to store a single 
# step for the upcoming
# pollard-rho algorithm
# it has a 'log' function
# to keep the code clean from
# thousands of 'print' statements

class Step:
    def __init__(self):
        self.i = 0
        self.x = 1
        self.alpha = 0
        self.beta = 0
        
    def log(self):
        print("Step %s:" % self.i)
        print(" i\tx\talpha\tbeta")
        print(" %d\t%d\t%d\t%d" % (self.i, self.x, self.alpha, self.beta))


# In[7]:


# calc_next_step function:
# this function calculates 
# the next step for the
# pollard-rho algorithm.
# input:
# Step-Class prev_step
# int a,g,p,n
# where p is a prime number and
# g is the generator either of Z*_p 
# or of a subgroup of Z*_p
# n is the order of g
# a is the value we want to
# find the discrete logarithm for
        
def calc_next_step(prev_step, a, g, p, n):
    
    group = prev_step.x % 3 
    x = prev_step.x
    alpha = prev_step.alpha
    beta = prev_step.beta
    
    # G1
    if group == 1:
        x = x * a % p
        alpha = alpha+1 % n
    
    # make sure, [1] is not in G2
    # G2
    if group == 2:
        x = x*x % p
        alpha = alpha*2 % n
        beta = beta*2 % n
    # G3
    if group == 0:
        x = x*g % p 
        beta = beta + 1 % n

    new_step = Step()
    new_step.i = prev_step.i + 1
    new_step.x = x
    new_step.alpha = alpha
    new_step.beta = beta
    return new_step


# In[8]:


# pollard_rho_v1 function:
# this function calculates
# the discrete logarithm using
# pollards rho algorithm
# input:
# int a,g,p, n
# where
# p is a prime number and 
# g is either a generator of Z*_p
# a generator of a subgroup of Z*_p
# n is the order of g
# if g generates Z*_p, n must not
# be set.
# returns the solution x for the
# discrete logarithm
# g^x = a mod p
# also returns three lists of single 
# steps. this function is used to show
# a visualisation of the algorithm.
# for calculations only please use the 
# pollard_rho_v2 function

def pollard_rho_v1(a, g ,p, n=None):
    print("starting pollards rho algorithm")
    print("looking for a solution x to solve: %s^x = %s mod %s" % (g, a, p))
    if p not in Primes():
        print("warning: %s is not a prime" %p)
    if not n:
        n = euler_phi(p)            
    
    if a == g:
        return 1
    tail = []
    circle1 = []
    circle2 = []
    
    
    # init the first two steps
    step_i = Step()
    steps = {0: Step()}
    
    # maps the steps to its x values
    x_to_steps = {0:1}
    
    for i in xrange(1, p):
        # step_i
        prev_step = steps[i-1]
        step_i = calc_next_step(prev_step, a,g,p,n)
        
        # print current step
        step_i.log()  
        
        x_i = step_i.x
        if x_i in x_to_steps:
            prev_i = x_to_steps[x_i]
            print(" collusion in step %s" %i)
            print(" x = %s has already occured in step %s" % (x_i, prev_i))
            print(" calculating s and t now:")
            
            step_j = steps[prev_i]
            # j: step with first appereance of current x_i
            # i: current step
            alpha_i = step_i.alpha
            alpha_j = step_j.alpha 
            beta_i = step_i.alpha 
            beta_j = step_j.beta
            
            s = alpha_j - alpha_i
            t = beta_i - beta_j
            print("found collusion at step i=%s" % i)
            print("   s = %s - %s = %s (mod %s)" % (alpha_j, alpha_i, s, n))
            print("   t = %s - %s = %s (mod %s)" % (beta_i, beta_j, t, n))
            
            
            #
            # create the visualisation values:
            
            if prev_i >2:
                tail = [steps[prev_i-2], steps[prev_i-1]]
            else:
                tail = [steps[prev_i-1]]
                
            k = prev_i
            while k < i:
                circle1.append(steps[k])
                k += 1
            
            circle2.append(step_i)
            # calculate steps for the second circle
            for circlestep in circle1[:-1]:
                c2_step = calc_next_step(step_i, a,g,p,n)
                circle2.append(c2_step)
                step_i = c2_step
            break
            
        # bookkeeping the current values
        steps[i] = step_i
        x_to_steps[x_i] = i
    
    solutions = solve_mod(int(s),int(t),int(n), interaction=True)
    if solutions == -1:
        print("cannot find the discrete logarithm. is the input correct?")
        return -1
    res = -1
    for sol in solutions:
        if g.powermod(sol, p) == a:
            print("found the discrete logarithm:")
            print("%s^%s = %s (mod %s)" % (g, sol, a, p)) 
            res = sol
            break
         
    return sol, tail, circle1, circle2


# In[23]:


# pollard_rho_v2 function:
# calculates the discrete logarithm
# using pollards rho algorithm
# without saving every step
# input:
# int a, g, p, n
# where:
# p is a prime number
# g is a generator of Z*_p
# or a subgroup of Z*_p
# n is the order of g and must only
# be used when calculating in a 
# subgroup. you can use the
# interaction-flag to get a more 
# detailed output of the single 
# calculations.
# returns the discrete logarithm x
# for which g^x = a mod p
# returns -1 if calculation failed

def pollard_rho_v2(a, g ,p, n=None, interaction=False):
    
    if p not in Primes():
        print("warning: %s is not a prime" %p)
    if not n:
        n = euler_phi(p)            
    
    if a == g:
        return 1
    
    if interaction:
        tail = []
        circle1 = []
        circle2 = []
    
    
    # init the first two steps
    step_i = Step()
    step_2i = calc_next_step(step_i, a, g ,p, n)
    
    i = 0
    while i < p:
        i += 1
        # step_i
        step_i = calc_next_step(step_i, a,g,p,n)
        
        # step_2i
        step_2i = calc_next_step(step_2i, a,g,p,n)
        step_2i = calc_next_step(step_2i, a,g,p,n)
        
        if step_i.x == step_2i.x:
            
            s = step_i.alpha-step_2i.alpha
            t = step_2i.beta- step_i.beta

            if interaction:
                print("found collusion at step i=%s" % i)
                print("   x_%s: %s = %s :x_%s" % (i, step_i.x, step_2i.x, 2*i))
                print("   s = %s - %s = %s (mod %s)" % (step_i.alpha, step_2i.alpha, s, n))
                print("   t = %s - %s = %s (mod %s)" % (step_2i.beta, step_i.beta, t, n))
            break
    
    solutions = solve_mod(int(s),int(t),int(n), interaction=interaction)
    if solutions == -1:
        print("could not find a solution")
        return -1
    for sol in solutions:
        if g.powermod(sol, p) == a:
            if interaction:
                print("found the discrete logarithm:")
                print("%s^%s = %s (mod %s)" % (g, sol, a, p)) 
            return sol 
    return -1


# In[10]:


# average_runtime function:
# calculates the average runtime of the algorithms
# by picking i random numbers in [0,...,p-1] and 
# solving the discrete logarithm for those numbers
# input:
# int g, p, i 
# where:
# p is a prime number and 
# g is a generator of Z*_p
# i is the number of tests to run.


def average_runtime(g,p,i, bf=True, bsgs=True, pr=True, ph=True):
    global delay
    print("average runtime testing")
    print(" doing %s loops, artificial delay is %s seconds" % (i,  delay))
    k = 0
    runtimes_bf = []
    runtimes_bsgs = []
    runtimes_rho = []
    runtimes_ph = []
    while k < i:
        k += 1
        # create random int
        a = randint(0,p-1)

        # start the brute-force test and append its time to the array
        if bf:
            start = time.time()
            brute_force(a, g, p)
            elapsed = time.time() - start
            runtimes_bf.append(elapsed)
            if elapsed > 60*90:
                bf = False

        # start the babystep-gianstep algorithm
        if bsgs:
            start = time.time()
            babystep_giantstep(a, g, p)
            elapsed = time.time() - start
            runtimes_bsgs.append(elapsed)
            if elapsed > 60*90:
                bsgs = False

        if pr:
            start = time.time()
            pollard_rho_v2(a, g, p)
            elapsed = time.time() - start
            runtimes_rho.append(elapsed)
            if elapsed > 60*90:
                pr = False
            
        if ph:
            start = time.time()
            pohlig_hellman_reduction(a, g, p)
            elapsed = time.time() - start
            runtimes_ph.append(elapsed)
            if elapsed > 60*90:
                ph = False
            
    print("finished test:")
    avg_bf = mean(runtimes_bf)
    avg_bsgs = mean(runtimes_bsgs)
    avg_rho = mean(runtimes_rho)
    avg_ph = mean(runtimes_ph)
    if bf:
        print("average time for brute-force:        %.5f" % avg_bf)
    if bsgs:
        print("average time for babystep-giantstep: %.5f" % avg_bsgs)
    if pr:
        print("average time for pollard-rho: %.5f" % avg_rho)
    if ph:
        print("average time for pohlig-hellmann: %.5f" % avg_ph)
    if bf and bsgs:
        print("babystep-giantstep is %0.2f times faster than brute forcing" % (avg_bf/avg_bsgs))
    if bf and pr:
        print("pollard-rho is %0.2f times faster than brute forcing" % (avg_bf/avg_rho))
    
    safe = "false"
    if (p-1)/2 in Primes():
        safe = "true"

        
    with open("testresults.csv", "a") as myFile:
        myFile.write("%s;%s;%s;%.5f;%.5f;%.5f;%.5f\n" % (g, p, safe, avg_bf,avg_bsgs,avg_rho,avg_ph))

    return


# In[11]:


# E X A M P L E S 

# 8 bit prime 
p_8 = 167    # = 83*2 + 1
g_8 = 5

# 22 bit prime 
p_22 = 2698727             # = 2 * 1349363 + 1
g_22 = 5

# 32 bit prime 
# pollard_rho: about 1 sec
# babystep: abot 1/2 sec
# bruteforce: about 1 hour
p_32= 4016930627                 # = 2 * 2008465313 + 1
g_32 = 2


# pollard_rho: about 4 minutes
# babystep: abot 2 minutes
# bruteforce: i dont know
p_49 = 676602320278583
g_49 = 5

# 64 bit prime 
# this crashed my laptop without a result
p_64 = 20311842717452181803        # = 2 * 10155921358726090901 +1
g_64 = 2



# Following prime was found in:
# Handbook of applied cryptography by Alfred J. Menezes, Paul C. van Oorschot and Scott A. Vanstone 
# http://cacr.uwaterloo.ca/hac/
# chapter 3, page 109
# you probably won't succeed to compute discrete logarithm with babystep-giantstep or pollard-rho
# but you can easily compute it using the pohlig_hellman_reduction()
p_353 = 22708823198678103974314518195029102158525052496759285596453269189798311427475159776411276642277139650833937
g_353 = 3


# In[12]:


# find group function:
# input: 
# int g
# returns a prime p 
# and an int g which is a
# generator of Z*_p. 


def find_group(n):
    success = False
    
    p = next_prime(n)
    
    div = divisors(p-1)
    
    # find a generator:
    for g in srange(2,20):
        counter = 0
        # a generator has order(n), so test
        # every possible order (= divisors of n).
        # [:-1] to delete n from divisors list 
        # because a^|G| = 1 for every a in G (Euler)
        for d in div[:-1]:
            if g.powermod(d, p) != 1:
                counter += 1
            else:
                break
        if counter == len(div) -1:
            if (p-1)/2 in Primes():
                print("%s is a safe prime" % p)
            print("found a generator of Z*_%s = < %s >" % (p, g))
            return g, p

    print("could not find a generator between 2 and 20 for prime %s" % p)
    return 0, 0
        


# In[20]:


# pohlig_hellman_prime_order function
# this function is used in the pohlig_hellman_reduction() 
# to compute the equations for every prime factor

def pohlig_hellman_prime_order(a, g, p, n, interaction=False):
    
    if n not in divisors(p-1):
        print("%s is not a divisor of %s!" %(n, (p-1)))
        return -1
    
    fac = factor(n)
    if len(list(fac)) > 1:
        print("your given n=%s is a product of more than 1 prime: %s" % (n, factor(n)))
        
    n = p-1
    q = fac[0][0]
    e = fac[0][1]
    if interaction:
        print("calculations for prime factor %s^%s of %s:" % (q,e, p-1))
    y = 1
    l = 0
    
    g_ = g.powermod((p-1)/q, p)
    if interaction:
        print("  generator of the subgroup is g_ = %s" % g_)
    x = 0
    j= 0
    while  j < e:        
        y = y * g.powermod((l * q**(j-1)), p)        
        a_ = a*y.powermod(-1, p)
        a_ = a_.powermod((n/(q**(j+1))), p)
        if interaction:
            print("  our corrosponding element in the subgroup  is a_ = %s" % a_)
            print("       %s^x = %s mod %s" % (g_, a_, p))
        x_ = pollard_rho_v2(a_, g_, p, q)
        if interaction:
            print("       -> x = %s (*%s^%s)" % (x_, q, j))
        l = x_
        x += x_*q**j
        
        j += 1
    if interaction:
        print("final result for prime factor %s is: %s" % (q, x))
        print("this leads to: x = %s mod %s\n" % (x, q))
    return x
    


# In[19]:


# pohlig_hellman_reduction() function:
# solves the discrete logarithm by 
# factorizing p-1 and solving the problem
# in those smaller groups using pollards-rho method


def pohlig_hellman_reduction(a,g,p, interaction=False):
    if interaction:
        print("-"*25)
        print("Pohling-Hellman algorithm")
        print("use factorization of %s  to compute discrete logarithm x with %s^x = %s mod %s" % (p-1, g,a,p))
        print("-"*25)
    
    if a == g:
        return 1
    chin_rem = []
    for f in list(factor(p-1)):
        n = f[0]**f[1]
        res = pohlig_hellman_prime_order(a, g, p, n, interaction=interaction)
        chin_rem.append([res, n])
    if interaction:
        print("-"*25)
        print("solve following equations with the chinese remainder:")
    m = []
    r = []
    for f in chin_rem:
        r.append(f[0])
        m.append(f[1])
        if interaction:
            print("    x = %s mod %s" % (f[0], f[1]))
    
    res = crt(r,m) % p
    if g.powermod(res, p) == a:
        if interaction:
            print("the solution for the discrete logarithm is: %s" % res)
            print("   %s^%s = %s mod %s" % (g,res,a,p))
        return res
    return -1
    


# In[22]:





# In[47]:


import random
# run_tests function:
# input: int k, low, high, size
# k: number of tests
# low: minimum size of primes
# high: maximum size of primes
# size: number of tests to run for each prime
# this function runs k runtime tests
# for brute force, babystep-giantstep,
# pollard-rho and pohlig-hellman 
# to calculate discrete logarithm


def run_tests(k, bits, size,  bf=False, bsgs=False, pr=False, ph=False):
    with open("testresults.csv", "a") as myFile:
        myFile.write("bits;samplesize;;;;;\n")
        myFile.write("%s;%s;;;;;\n" %(bits, size))
        myFile.write("g;p;safe_prime;brute_force;babystep;pollard_rho;pohlig-hellmann\n")
        
    i = 0
    while i < k:
        i += 1
        a = random.randint(2**bits, 2**(bits+1)-1)
        g, p = find_group(a)
        if p != 0:  
            average_runtime(g, p, size, bf=bf, bsgs=bsgs, pr=pr, ph=ph)



# In[58]:


# run_tests(5, low=2**22, high=2**23-1, size=5, bf=True, bsgs=True, pr=True, ph=True)


# In[57]:


def auto_test():
    bf=False
    bsgs=False
    pr=True
    ph=True
    for i in srange(2,120):
        b = 4*i
        
        if b > 28:
            # for primes > 2^28 it takes too long to brute force it
            bf = False
        if b > 44:
            # for primes > 2^28 it takes too much RAM 
            bsgs = False
            
        run_tests(25, bits=b, size=50, bf=bf, bsgs=bsgs, pr=pr, ph=ph)


# In[ ]:


auto_test()


